const httpResponse = require("./http-response/http-response.method");
const { send, responseHandler } = require("./http-response/responseHandler");

module.exports = {
  send,
  responseHandler,
  ...httpResponse,
};
