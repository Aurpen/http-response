const httpEnum = require("./http-reponse.enum");

const Continue = (obj = {}) => ({ ...httpEnum["CONTINUE"], ...obj });
const SwitchingProtocols = (obj = {}) => ({
  ...httpEnum["SWITCHING_PROTOCOLS"],
  ...obj,
});
const Processing = (obj = {}) => ({ ...httpEnum["PROCESSING"], ...obj });
const EarlyHints = (obj = {}) => ({ ...httpEnum["EARLY_HINTS"], ...obj });
const OK = (obj = {}) => ({ ...httpEnum["OK"], ...obj });
const Created = (obj = {}) => ({ ...httpEnum["CREATED"], ...obj });
const Accepted = (obj = {}) => ({ ...httpEnum["ACCEPTED"], ...obj });
const NonAuthoritativeInformation = (obj = {}) => ({
  ...httpEnum["NON_AUTHORITATIVE_INFORMATION"],
  ...obj,
});
const NoContent = (obj = {}) => ({ ...httpEnum["NO_CONTENT"], ...obj });
const ResetContent = (obj = {}) => ({ ...httpEnum["RESET_CONTENT"], ...obj });
const PartialContent = (obj = {}) => ({
  ...httpEnum["PARTIAL_CONTENT"],
  ...obj,
});
const MultiStatus = (obj = {}) => ({ ...httpEnum["MULTI_STATUS"], ...obj });
const AlreadyReported = (obj = {}) => ({
  ...httpEnum["ALREADY_REPORTED"],
  ...obj,
});
const IMUsed = (obj = {}) => ({ ...httpEnum["IM_USED"], ...obj });
const MultipleChoices = (obj = {}) => ({
  ...httpEnum["MULTIPLE_CHOICES"],
  ...obj,
});
const MovedPermanently = (obj = {}) => ({
  ...httpEnum["MOVED_PERMANENTLY"],
  ...obj,
});
const Found = (obj = {}) => ({ ...httpEnum["FOUND"], ...obj });
const SeeOther = (obj = {}) => ({ ...httpEnum["SEE_OTHER"], ...obj });
const NotModified = (obj = {}) => ({ ...httpEnum["NOT_MODIFIED"], ...obj });
const UseProxy = (obj = {}) => ({ ...httpEnum["USE_PROXY"], ...obj });
const TemporaryRedirect = (obj = {}) => ({
  ...httpEnum["TEMPORARY_REDIRECT"],
  ...obj,
});
const PermanentRedirect = (obj = {}) => ({
  ...httpEnum["PERMANENT_REDIRECT"],
  ...obj,
});
const BadRequest = (obj = {}) => ({ ...httpEnum["BAD_REQUEST"], ...obj });
const Unauthorized = (obj = {}) => ({ ...httpEnum["UNAUTHORIZED"], ...obj });
const PaymentRequired = (obj = {}) => ({
  ...httpEnum["PAYMENT_REQUIRED"],
  ...obj,
});
const Forbidden = (obj = {}) => ({ ...httpEnum["FORBIDDEN"], ...obj });
const NotFound = (obj = {}) => ({ ...httpEnum["NOT_FOUND"], ...obj });
const MethodNotAllowed = (obj = {}) => ({
  ...httpEnum["METHOD_NOT_ALLOWED"],
  ...obj,
});
const NotAcceptable = (obj = {}) => ({ ...httpEnum["NOT_ACCEPTABLE"], ...obj });
const ProxyAuthenticationRequired = (obj = {}) => ({
  ...httpEnum["PROXY_AUTHENTICATION_REQUIRED"],
  ...obj,
});
const RequestTimeout = (obj = {}) => ({
  ...httpEnum["REQUEST_TIMEOUT"],
  ...obj,
});
const Conflict = (obj = {}) => ({ ...httpEnum["CONFLICT"], ...obj });
const Gone = (obj = {}) => ({ ...httpEnum["GONE"], ...obj });
const LengthRequired = (obj = {}) => ({
  ...httpEnum["LENGTH_REQUIRED"],
  ...obj,
});
const PreconditionFailed = (obj = {}) => ({
  ...httpEnum["PRECONDITION_FAILED"],
  ...obj,
});
const PayloadTooLarge = (obj = {}) => ({
  ...httpEnum["PAYLOAD_TOO_LARGE"],
  ...obj,
});
const URITooLong = (obj = {}) => ({ ...httpEnum["URI_TOO_LONG"], ...obj });
const UnsupportedMediaType = (obj = {}) => ({
  ...httpEnum["UNSUPPORTED_MEDIA_TYPE"],
  ...obj,
});
const RangeNotSatisfiable = (obj = {}) => ({
  ...httpEnum["RANGE_NOT_SATISFIABLE"],
  ...obj,
});
const ExpectationFailed = (obj = {}) => ({
  ...httpEnum["EXPECTATION_FAILED"],
  ...obj,
});
const ImaTeapot = (obj = {}) => ({ ...httpEnum["IM_A_TEAPOT"], ...obj });
const MisdirectedRequest = (obj = {}) => ({
  ...httpEnum["MISDIRECTED_REQUEST"],
  ...obj,
});
const UnprocessableEntity = (obj = {}) => ({
  ...httpEnum["UNPROCESSABLE_ENTITY"],
  ...obj,
});
const Locked = (obj = {}) => ({ ...httpEnum["LOCKED"], ...obj });
const FailedDependency = (obj = {}) => ({
  ...httpEnum["FAILED_DEPENDENCY"],
  ...obj,
});
const TooEarly = (obj = {}) => ({ ...httpEnum["TOO_EARLY"], ...obj });
const UpgradeRequired = (obj = {}) => ({
  ...httpEnum["UPGRADE_REQUIRED"],
  ...obj,
});
const PreconditionRequired = (obj = {}) => ({
  ...httpEnum["PRECONDITION_REQUIRED"],
  ...obj,
});
const TooManyRequests = (obj = {}) => ({
  ...httpEnum["TOO_MANY_REQUESTS"],
  ...obj,
});
const RequestHeaderFieldsTooLarge = (obj = {}) => ({
  ...httpEnum["REQUEST_HEADER_FIELDS_TOO_LARGE"],
  ...obj,
});
const UnavailableForLegalReasons = (obj = {}) => ({
  ...httpEnum["UNAVAILABLE_FOR_LEGAL_REASONS"],
  ...obj,
});
const SSLCertificateError = (obj = {}) => ({
  ...httpEnum["SSL_CERTIFICATE_ERROR"],
  ...obj,
});
const InternalServerError = (obj = {}) => ({
  ...httpEnum["INTERNAL_SERVER_ERROR"],
  ...obj,
});
const NotImplemented = (obj = {}) => ({
  ...httpEnum["NOT_IMPLEMENTED"],
  ...obj,
});
const BadGateway = (obj = {}) => ({ ...httpEnum["BAD_GATEWAY"], ...obj });
const ServiceUnavailable = (obj = {}) => ({
  ...httpEnum["SERVICE_UNAVAILABLE"],
  ...obj,
});
const GatewayTimeout = (obj = {}) => ({
  ...httpEnum["GATEWAY_TIMEOUT"],
  ...obj,
});
const HTTPVersionNotSupported = (obj = {}) => ({
  ...httpEnum["HTTP_VERSION_NOT_SUPPORTED"],
  ...obj,
});
const VariantAlsoNegotiates = (obj = {}) => ({
  ...httpEnum["VARIANT_ALSO_NEGOTIATES"],
  ...obj,
});
const InsufficientStorage = (obj = {}) => ({
  ...httpEnum["INSUFFICIENT_STORAGE"],
  ...obj,
});
const LoopDetected = (obj = {}) => ({ ...httpEnum["LOOP_DETECTED"], ...obj });
const BandwidthLimitExceeded = (obj = {}) => ({
  ...httpEnum["BANDWIDTH_LIMIT_EXCEEDED"],
  ...obj,
});
const NotExtended = (obj = {}) => ({ ...httpEnum["NOT_EXTENDED"], ...obj });
const NetworkAuthenticationRequired = (obj = {}) => ({
  ...httpEnum["NETWORK_AUTHENTICATION_REQUIRED"],
  ...obj,
});

module.exports = {
  Continue,
  SwitchingProtocols,
  Processing,
  EarlyHints,
  OK,
  Created,
  Accepted,
  NonAuthoritativeInformation,
  NoContent,
  ResetContent,
  PartialContent,
  MultiStatus,
  AlreadyReported,
  IMUsed,
  MultipleChoices,
  MovedPermanently,
  Found,
  SeeOther,
  NotModified,
  UseProxy,
  TemporaryRedirect,
  PermanentRedirect,
  BadRequest,
  Unauthorized,
  PaymentRequired,
  Forbidden,
  NotFound,
  MethodNotAllowed,
  NotAcceptable,
  ProxyAuthenticationRequired,
  RequestTimeout,
  Conflict,
  Gone,
  LengthRequired,
  PreconditionFailed,
  PayloadTooLarge,
  URITooLong,
  UnsupportedMediaType,
  RangeNotSatisfiable,
  ExpectationFailed,
  ImaTeapot,
  MisdirectedRequest,
  UnprocessableEntity,
  Locked,
  FailedDependency,
  TooEarly,
  UpgradeRequired,
  PreconditionRequired,
  TooManyRequests,
  RequestHeaderFieldsTooLarge,
  UnavailableForLegalReasons,
  SSLCertificateError,
  InternalServerError,
  NotImplemented,
  BadGateway,
  ServiceUnavailable,
  GatewayTimeout,
  HTTPVersionNotSupported,
  VariantAlsoNegotiates,
  InsufficientStorage,
  LoopDetected,
  BandwidthLimitExceeded,
  NotExtended,
  NetworkAuthenticationRequired,
};
