class ResponseSender {
  constructor() {}

  /**
   * Sender of response
   * @param {Object} response
   */
  send = (response) => {
    const { code } = response;
    this.res.status(code ? code : 500);
    this.res.send(response);
  };

  /**
   * Middleware stter of sender
   * @param {Request} req
   * @param {Response} res
   * @param {function} next
   */
  responseHandler = (req, res, next) => {
    this.res = res;
    next();
  };
}

module.exports = new ResponseSender();
